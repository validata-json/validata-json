# Validata JSON

Plateforme de validation de données ouvertes au format GeoJSON, avec les 
spécificités suivantes :

- API permettant la validation asynchrone de fichiers GeoJSON via des schémas 
  jsonschema Draft 7 (données locales ou en ligne).
- Outil en ligne de commande
- Possibilité d'obtenir des rapports au format texte ou json, avec des 
  messages d'erreur complets et en français.
- Gestion des fichiers volumineux en découpant le fichier en plusieurs 
  morceaux validés séparément (découpage du tableau "features")
  
Le service est accessible à l'adresse suivante : 
https://json.validator.validata.fr

Consultez [la documentation de l'API](https://json.validator.validata.fr/docs) 
pour en savoir plus.

Exemples de requêtes avec `curl` (remplacer les uuid avec l'identifiant 
récupéré au premier appel) :

```shell
# 🚀 Lancer la tâche de validation sur un fichier d'aménagements cyclables
curl -X 'POST' \
  'https://json.validator.validata.fr/url_validation_job?data=https%3A%2F%2Fwww.data.gouv.fr%2Ffr%2Fdatasets%2Fr%2F54f9ee93-76e4-4e02-afbf-7dd0c954355a&schema=https%3A%2F%2Fschema.data.gouv.fr%2Fschemas%2Fetalab%2Fschema-amenagements-cyclables%2F0.3.3%2Fschema_amenagements_cyclables.json' \
  -H 'accept: application/json' \
  -d ''
# Exemple de réponse : 29abee43-99ec-4d17-9699-9a7ff61f9d13

# 🔎 Vérifier le statut de la tâche de validation
curl -X 'GET' \
  'https://json.validator.validata.fr/job/29abee43-99ec-4d17-9699-9a7ff61f9d13' \
  -H 'accept: application/json'
# Exemple de réponse : {"job_status": {"json_split": "SUCCESS", "validation": "PENDING"}}

# 📜 Récupérer le résultat une fois le rapport disponible
curl -X 'GET' \
  'https://json.validator.validata.fr/job/29abee43-99ec-4d17-9699-9a7ff61f9d13/output?text_or_json=text' \
  -H 'accept: application/json'
# Exemple de réponse : { "validation_report": "Erreur survenue au niveau de l'attribut \"geometry\"\nPosition : (racine).features[0].geometry...
```

Selon le client, le deuxième appel redirigera vers le résultat du troisième si 
les calculs sont achevés.

## Tester en local

Pour interagir avec l'API en environnement de développement:

* Cloner le répertoire
* Se déplacer dans le répertoire ```cd validata-json```
* Lancer le serveur avec ```docker compose up --build``` 

Vous pouvez alors faire les requêtes sur `localhost:8000`.

Pour lancer les tests, il est recommandé d'utiliser 
[`poetry`](https://python-poetry.org/):

```shell
poetry install
make test
```

# Guide de déploiement

Ce guide d'installation vaut pour une machine Ubuntu.

## Prérequis

Installation des paquets système :

```shell
apt-get update
apt-get install git
```

## Création de l'utilisateur validata-json

- Ajouter le nouvel utilisateur `validata-json`

```shell
useradd -m validata-json
```

- Suppression du mot de passe pour le nouvel utilisateur

```shell
passwd -d validata-json
```

- Ajout de l'utilisateur aux sudoers

```shell
usermod -aG sudo validata-json
```


- NB:
  - changer d'utilisateur : `su - validata-json`
  - revenir à l'utilisateur root : (Ctrl + D) ou exit

Pour les commandes suivantes, s'assurer d'être connecté en tant que `validata-json`


## Installation de docker et docker-compose

Se référer à la [procédure officielle](https://docs.docker.com/engine/install/ubuntu/)

## Clonage du dépôt

```shell
git clone https://gitlab.com/validata-json/validata-json.git
```

## Configuration d'un reverse proxy

Il est possible d'utiliser nginx avec certbot pour configurer un reverse proxy.

## Lancement du serveur en daemon

Dans le dossier validata-json :

```shell
docker compose up -d --build
```

## Suivi des logs

```shell
docker compose logs -f
```

