============
Installation
============

At the command line either via easy_install or pip::

    $ easy_install json.validata
    $ pip install json.validata

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv json.validata
    $ pip install json.validata
