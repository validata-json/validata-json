openapi: 3.0.3
info:
  title: JSON Validation Service
  description: A service for validating JSON files with respect to a given JSON Schema.
  version: "1.0"
servers:
- url: https://json.validator.validata.fr/
  description: Hosts the JSON validation service.
- url: http://localhost:8000/
  description: Hosts the JSON validation service.
paths:
  /validate:
    get:
      tags:
      - validation
      summary: Validate data with URL
      description: Validation of remote data with a remote JSON schema
      operationId: validation_validate_url
      parameters:
      - name: data
        in: query
        description: JSON file with data to validate, MUST be in the format of an
          URL
        allowEmptyValue: true
        required: true
        schema:
          type: string
          description: JSON file with data to validate, MUST be in the format of an
            URL
          example: https://www.data.gouv.fr/fr/datasets/r/9ca17d67-3ba3-410b-9e32-6ac7948c3e06
        example: https://www.data.gouv.fr/fr/datasets/r/9ca17d67-3ba3-410b-9e32-6ac7948c3e06
      - name: schema
        in: query
        description: JSON Schema file, MUST be in the format of an URL
        allowEmptyValue: true
        required: true
        schema:
          type: string
          description: JSON Schema file, MUST be in the format of an URL
          example: https://schema.data.gouv.fr/schemas/etalab/schema-amenagements-cyclables/0.3.3/schema_amenagements_cyclables.json
        example: https://schema.data.gouv.fr/schemas/etalab/schema-amenagements-cyclables/0.3.3/schema_amenagements_cyclables.json
      - name: raw
        in: query
        description: Should the raw errors be returned in addition to the report.
          Beware, can result in large return value.
        allowEmptyValue: true
        schema:
          type: boolean
          description: Should the raw errors be returned in addition to the report.
            Beware, can result in large return value.
          default: false
          example: false
        example: false
      responses:
        "200":
          description: OK response.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ValidationResult'
              example:
                ValidationErrors:
                - context: (racine).foo.0.bar
                  description: Le tableau contient des éléments supplémentaires à
                    ceux autorisés par le schéma.
                  details:
                    Deleniti delectus eos repellendus.: Deleniti voluptatem deleniti
                      ut dignissimos doloremque.
                    Veritatis fuga dolor animi blanditiis sed nesciunt.: Sunt tenetur
                      facere et.
                  field: bar
                  type: array_no_additional_items
                  value: '[1,2,3,4]'
                - context: (racine).foo.0.bar
                  description: Le tableau contient des éléments supplémentaires à
                    ceux autorisés par le schéma.
                  details:
                    Deleniti delectus eos repellendus.: Deleniti voluptatem deleniti
                      ut dignissimos doloremque.
                    Veritatis fuga dolor animi blanditiis sed nesciunt.: Sunt tenetur
                      facere et.
                  field: bar
                  type: array_no_additional_items
                  value: '[1,2,3,4]'
                ValidationSummary: "Erreur survenue au niveau de l'attribut \"bar\"\nPosition
                  \               : (racine).foo.0.bar\nDescription de l'erreur :
                  Le tableau contient des éléments supplémentaires à ceux autorisés
                  par le schéma.\nValeur de l'objet       : [1,2,3,4]\nCode erreur
                  \            : array_no_additional_items\nErreur similaire sur cet
                  attribut constatée à 1 autre(s) position(s)\n\t(racine).foo.3.bar\n"
                isValid: false
        "415":
          description: Unsupported Media Type response.
          content:
            application/vnd.goa.error:
              schema:
                $ref: '#/components/schemas/Error'
              example:
                id: 3F1FKVRR
                message: Value of ID must be an integer
                name: bad_request
        "417":
          description: Expectation Failed response.
          content:
            application/vnd.goa.error:
              schema:
                $ref: '#/components/schemas/Error'
              example:
                id: 3F1FKVRR
                message: Value of ID must be an integer
                name: bad_request
    post:
      tags:
      - validation
      summary: Validate local data
      description: Validation of a local data file with a remote JSON schema
      operationId: validation_validate_file
      requestBody:
        required: true
        content:
          multipart/form-data:
            schema:
              $ref: '#/components/schemas/ValidateFileRequestBody'
            example:
              data: '@local_data.json'
              raw: false
              schema: https://schema.data.gouv.fr/schemas/etalab/schema-amenagements-cyclables/0.3.3/schema_amenagements_cyclables.json
      responses:
        "200":
          description: OK response.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ValidationResult'
              example:
                ValidationErrors:
                - context: (racine).foo.0.bar
                  description: Le tableau contient des éléments supplémentaires à
                    ceux autorisés par le schéma.
                  details:
                    Deleniti delectus eos repellendus.: Deleniti voluptatem deleniti
                      ut dignissimos doloremque.
                    Veritatis fuga dolor animi blanditiis sed nesciunt.: Sunt tenetur
                      facere et.
                  field: bar
                  type: array_no_additional_items
                  value: '[1,2,3,4]'
                - context: (racine).foo.0.bar
                  description: Le tableau contient des éléments supplémentaires à
                    ceux autorisés par le schéma.
                  details:
                    Deleniti delectus eos repellendus.: Deleniti voluptatem deleniti
                      ut dignissimos doloremque.
                    Veritatis fuga dolor animi blanditiis sed nesciunt.: Sunt tenetur
                      facere et.
                  field: bar
                  type: array_no_additional_items
                  value: '[1,2,3,4]'
                ValidationSummary: "Erreur survenue au niveau de l'attribut \"bar\"\nPosition
                  \               : (racine).foo.0.bar\nDescription de l'erreur :
                  Le tableau contient des éléments supplémentaires à ceux autorisés
                  par le schéma.\nValeur de l'objet       : [1,2,3,4]\nCode erreur
                  \            : array_no_additional_items\nErreur similaire sur cet
                  attribut constatée à 1 autre(s) position(s)\n\t(racine).foo.3.bar\n"
                isValid: false
        "415":
          description: Unsupported Media Type response.
          content:
            application/vnd.goa.error:
              schema:
                $ref: '#/components/schemas/Error'
              example:
                id: 3F1FKVRR
                message: Value of ID must be an integer
                name: bad_request
        "417":
          description: Expectation Failed response.
          content:
            application/vnd.goa.error:
              schema:
                $ref: '#/components/schemas/Error'
              example:
                id: 3F1FKVRR
                message: Value of ID must be an integer
                name: bad_request
components:
  schemas:
    Error:
      type: object
      properties:
        fault:
          type: boolean
          description: Is the error a server-side fault?
          example: false
        id:
          type: string
          description: ID is a unique identifier for this particular occurrence of
            the problem.
          example: 123abc
        message:
          type: string
          description: Message is a human-readable explanation specific to this occurrence
            of the problem.
          example: parameter 'p' must be an integer
        name:
          type: string
          description: Name is the name of this class of errors.
          example: bad_request
        temporary:
          type: boolean
          description: Is the error temporary?
          example: true
        timeout:
          type: boolean
          description: Is the error a timeout?
          example: true
      description: UnprocessableJsonSchema is the error returned when the JSON Schema
        provided is valid json, but does not follow JSON Schema specifications.
      example:
        id: 3F1FKVRR
        message: Value of ID must be an integer
        name: bad_request
      required:
      - name
      - id
      - message
      - temporary
      - timeout
      - fault
    ValidateFileRequestBody:
      type: object
      properties:
        data:
          type: string
          description: JSON file with data to validate
          example: '@local_data.json'
        raw:
          type: boolean
          description: Should the raw errors be returned in addition to the report.
            Beware, can result in large return value.
          default: false
          example: false
        schema:
          type: string
          description: JSON Schema file, MUST be in the format of an URL
          example: https://schema.data.gouv.fr/schemas/etalab/schema-amenagements-cyclables/0.3.3/schema_amenagements_cyclables.json
      example:
        data: '@local_data.json'
        raw: false
        schema: https://schema.data.gouv.fr/schemas/etalab/schema-amenagements-cyclables/0.3.3/schema_amenagements_cyclables.json
      required:
      - data
      - schema
    ValidationErrorCollection:
      type: array
      items:
        $ref: '#/components/schemas/Validationerror'
      example:
      - context: (racine).foo.0.bar
        description: Le tableau contient des éléments supplémentaires à ceux autorisés
          par le schéma.
        details:
          Deleniti delectus eos repellendus.: Deleniti voluptatem deleniti ut dignissimos
            doloremque.
          Veritatis fuga dolor animi blanditiis sed nesciunt.: Sunt tenetur facere
            et.
        field: bar
        type: array_no_additional_items
        value: '[1,2,3,4]'
      - context: (racine).foo.0.bar
        description: Le tableau contient des éléments supplémentaires à ceux autorisés
          par le schéma.
        details:
          Deleniti delectus eos repellendus.: Deleniti voluptatem deleniti ut dignissimos
            doloremque.
          Veritatis fuga dolor animi blanditiis sed nesciunt.: Sunt tenetur facere
            et.
        field: bar
        type: array_no_additional_items
        value: '[1,2,3,4]'
      - context: (racine).foo.0.bar
        description: Le tableau contient des éléments supplémentaires à ceux autorisés
          par le schéma.
        details:
          Deleniti delectus eos repellendus.: Deleniti voluptatem deleniti ut dignissimos
            doloremque.
          Veritatis fuga dolor animi blanditiis sed nesciunt.: Sunt tenetur facere
            et.
        field: bar
        type: array_no_additional_items
        value: '[1,2,3,4]'
      - context: (racine).foo.0.bar
        description: Le tableau contient des éléments supplémentaires à ceux autorisés
          par le schéma.
        details:
          Deleniti delectus eos repellendus.: Deleniti voluptatem deleniti ut dignissimos
            doloremque.
          Veritatis fuga dolor animi blanditiis sed nesciunt.: Sunt tenetur facere
            et.
        field: bar
        type: array_no_additional_items
        value: '[1,2,3,4]'
    ValidationResult:
      type: object
      properties:
        ValidationErrors:
          $ref: '#/components/schemas/ValidationErrorCollection'
        ValidationSummary:
          type: string
          description: Summary of validation errors.
          example: "Erreur survenue au niveau de l'attribut \"bar\"\nPosition                :
            (racine).foo.0.bar\nDescription de l'erreur : Le tableau contient des
            éléments supplémentaires à ceux autorisés par le schéma.\nValeur de l'objet
            \      : [1,2,3,4]\nCode erreur             : array_no_additional_items\nErreur
            similaire sur cet attribut constatée à 1 autre(s) position(s)\n\t(racine).foo.3.bar\n"
        isValid:
          type: boolean
          description: Whether the input JSON is valid
          example: true
      example:
        ValidationErrors:
        - context: (racine).foo.0.bar
          description: Le tableau contient des éléments supplémentaires à ceux autorisés
            par le schéma.
          details:
            Deleniti delectus eos repellendus.: Deleniti voluptatem deleniti ut dignissimos
              doloremque.
            Veritatis fuga dolor animi blanditiis sed nesciunt.: Sunt tenetur facere
              et.
          field: bar
          type: array_no_additional_items
          value: '[1,2,3,4]'
        - context: (racine).foo.0.bar
          description: Le tableau contient des éléments supplémentaires à ceux autorisés
            par le schéma.
          details:
            Deleniti delectus eos repellendus.: Deleniti voluptatem deleniti ut dignissimos
              doloremque.
            Veritatis fuga dolor animi blanditiis sed nesciunt.: Sunt tenetur facere
              et.
          field: bar
          type: array_no_additional_items
          value: '[1,2,3,4]'
        ValidationSummary: "Erreur survenue au niveau de l'attribut \"bar\"\nPosition
          \               : (racine).foo.0.bar\nDescription de l'erreur : Le tableau
          contient des éléments supplémentaires à ceux autorisés par le schéma.\nValeur
          de l'objet       : [1,2,3,4]\nCode erreur             : array_no_additional_items\nErreur
          similaire sur cet attribut constatée à 1 autre(s) position(s)\n\t(racine).foo.3.bar\n"
        isValid: true
      required:
      - isValid
      - ValidationErrors
      - ValidationSummary
    Validationerror:
      type: object
      properties:
        context:
          type: string
          description: Tree like notation of the part that failed the validation
          example: (racine).foo.0.bar
        description:
          type: string
          description: Human-readable description of the error
          example: Le tableau contient des éléments supplémentaires à ceux autorisés
            par le schéma.
        details:
          type: object
          description: Additional details about the error
          example:
            Consequatur vitae esse voluptatem alias asperiores minima.: Illo commodi
              mollitia sunt quasi at.
            Sed laborum molestiae quo laborum.: Odit sit quis voluptas harum.
          additionalProperties:
            type: string
            example: Dicta magni asperiores id in qui.
            format: binary
        field:
          type: string
          description: Name of field that triggered an error
          example: bar
        type:
          type: string
          description: Type of error
          example: array_no_additional_items
        value:
          type: string
          description: Value of the field related to the error
          example: '[1,2,3,4]'
          format: binary
      description: All information to inspect a specific validation error.
      example:
        context: (racine).foo.0.bar
        description: Le tableau contient des éléments supplémentaires à ceux autorisés
          par le schéma.
        details:
          Et ipsa.: Quia ex vero et.
        field: bar
        type: array_no_additional_items
        value: '[1,2,3,4]'
      required:
      - field
      - type
      - context
      - description
      - value
      - details
tags:
- name: validation
  description: The validation service validates JSON Files with respect to a given
    JSON Schema
