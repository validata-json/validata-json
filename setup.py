#!/usr/bin/env python

import os
import sys

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

from json_validata.__version__ import __version__
from json_validata.__init__ import __author__, __email__


if sys.argv[-1] == 'publish':
    os.system('python setup.py sdist upload')
    sys.exit()

readme = open('README.rst').read()
doclink = """
Documentation
-------------

The full documentation is at http://json.validata.rtfd.org."""
history = open('HISTORY.rst').read().replace('.. :changelog:', '')

setup(
    name='json_validata',
    version=__version__,
    description='Plateforme de validation de données ouvertes au format GeoJSON',
    long_description=readme + '\n\n' + doclink + '\n\n' + history,
    author=__author__,
    author_email=__email__,
    url='https://github.com/multi-coop/json.validata',
    packages=[
        'json_validata',
    ],
    package_dir={'json_validata': 'json_validata'},
    include_package_data=True,
    install_requires=[
    ],
    license='MIT',
    zip_safe=False,
    keywords='json.validata',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: Implementation :: PyPy',
    ],
)
