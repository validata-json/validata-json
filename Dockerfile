# Install dependencies with poetry
FROM python:3.9 AS poetry

ENV POETRY_VERSION="1.1.14"
ENV POETRY_HOME="/opt/poetry"
ENV POETRY_VIRTUALENVS_IN_PROJECT=true
ENV PATH="$POETRY_HOME/bin:$PATH"

WORKDIR /code

COPY ./poetry.lock /code/poetry.lock
COPY ./pyproject.toml /code/pyproject.toml
RUN pip install --no-cache-dir --upgrade "poetry==$POETRY_VERSION"
RUN poetry install --no-dev

# Copy virtual environment with dependencies on smaller image
FROM python:3.9-slim AS runtime

WORKDIR /code

COPY ./json_validata/ /code/json_validata
COPY --from=poetry /code /code

ENV PYTHONPATH="${PYTHONPATH}:/code/"
ENV PATH="/code/.venv/bin:$PATH"
ENV CELERY_BACKEND="redis://broker:6379"
ENV CELERY_BROKER="redis://broker:6379"

# Launch the API service
FROM runtime AS api

EXPOSE 8080
CMD ["uvicorn", "--app-dir", "json_validata/api", "main:app", "--host", "0.0.0.0", "--port", "8000"]

# Launch the celery workers service
FROM runtime AS worker
CMD ["celery", "--workdir", "json_validata/task_queue/", "-A", "server", "worker", "--loglevel=INFO"]
