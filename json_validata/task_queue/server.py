import os

from celery import Celery

celery_backend = os.environ.get('CELERY_BACKEND') \
    if os.environ.get('CELERY_BACKEND') \
    else 'redis://'
celery_broker = os.environ.get('CELERY_BROKER') \
    if os.environ.get('CELERY_BROKER') \
    else 'redis://'

app = Celery(
    'validation_jobs',
    backend=celery_backend,
    broker=celery_broker,
    include=['json_validata.task_queue.tasks']
)


if __name__ == "__main__":
    app.start()
