import os
from io import TextIOWrapper
import json
from typing import List, Dict

from celery import result, chord, signature

from json_validata.api.models import Metadata
from json_validata.report import report
from json_validata.streaming_processing import json_splitter
from json_validata.task_queue.server import app


def queue_job(
    schema_file: TextIOWrapper,
    data_path: str,
    metadata: Metadata,
    batch_size: int = 1000,
) -> str:
    """
    Takes a schema and data and queues the asynchronous validation job.

    `schema_file` is expected to be a file object supporting ".read()".
    `data_path` is expected to be the path to a local data file.

    Technical details:
    ---

    The job can be decomposed in three phases:
    - split the data into smaller chunks of size `batch_size`
    - validate each chunk separately
    - aggregate the results into a single report

    This job queues the first task, during which the two following tasks are
    queued together as a callback (with a celery "chord" canvas).

    Return:
    ---
        str: Id of the AsyncResult of the splitting task. The result of this
        task is then the id of the AsyncResult of the two subsequent tasks.
    """
    schema = json.load(schema_file)
    split_job = split_json_task.delay(
        data_path,
        batch_size,
        validation_task.s(schema=schema),
        aggregation_task.s(metadata=metadata.dict())
    )
    return split_job.id


@app.task(max_retries=1)
def split_json_task(
    data_path,
    batch_size,
    callback_validate,
    callback_aggregate
):
    """ Splits the data and queues a "chord" pipeline on the batches

    Returns the id of the AsyncResult of the chord, which allows fetching the
    final result.
    """
    try:
        # Deserialize celery tasks
        callback_validate = signature(callback_validate)
        callback_aggregate = signature(callback_aggregate)

        with open(data_path, 'rb') as data_file:

            batch_iterator = json_splitter.split_json(data_file, batch_size)

            # Create validation subtasks
            task_iterator = (
                callback_validate.clone(
                    kwargs={'instance': batch, 'index_shift': batch_size * i}
                )
                for (i, batch) in enumerate(batch_iterator)
                )

            # Finally run the task chord performing validation followed by
            # aggregation of the results
            batches_pipeline = chord(task_iterator)(callback_aggregate)
    finally:
        os.remove(data_path)

    return batches_pipeline.id


# `validation_task` registers `report.validate` as a task to celery
validation_task = app.task(trail=True, max_retries=1)(report.validate_to_json)


# `aggregation_task` registers `report.Report.merge` as a task to celery
aggregation_task = app.task(max_retries=1)(report.merge_reports)


@app.task(max_retries=1)
def aggregation_task(json_reports: List[str], metadata: Dict):
    return {
        'report': report.merge_reports(json_reports),
        'metadata': metadata
    }


def fetch_validation_job_status(job_id, app):
    """
    Retrieve the AsyncResult object thanks to an id and return the status
    of the workflow.

    Return: str
        Status of the AsyncResult. {'PENDING', 'STARTED', 'RETRY', 'FAILURE',
        'SUCCESS'}
    """
    status_dict = {}
    split_job = result.AsyncResult(job_id, app=app)
    status_dict['json_split_status'] = split_job.status
    if status_dict['json_split_status'] == 'SUCCESS':
        chord_job_id = split_job.get()
        chord_job = result.AsyncResult(chord_job_id, app=app)
        status_dict['validation_status'] = chord_job.status
    else:
        status_dict['validation_status'] = 'UNKNOWN'
    return status_dict


def get_validation_report(job_id: str, app):
    """
    Get the result of the workflow. If the result is not ready yet, the
    function waits for it.

    Return: str
        Report of validation.
    """
    chord_job_id = result.AsyncResult(job_id, app=app).get()
    chord_job = result.AsyncResult(chord_job_id, app=app)
    return chord_job.get()
