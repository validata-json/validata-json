import io
import os
from urllib import request
import shutil
import tempfile
from typing import Union

from starlette.datastructures import UploadFile


def _to_temporary_file(
    data: Union[str, UploadFile],
    data_is_remote: bool
) -> io.BufferedIOBase:
    """ Returns the path to a temporary file
    """
    if data_is_remote:
        data_file = request.urlopen(data)
    else:
        data_file = data

    file_desc, name = tempfile.mkstemp(prefix="validata")
    with os.fdopen(file_desc, 'wb') as f:
        shutil.copyfileobj(data_file, f)
    return name
