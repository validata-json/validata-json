from __future__ import annotations
from pydantic import HttpUrl
from tempfile import SpooledTemporaryFile
from typing import Union, Literal
from urllib import request

from fastapi import FastAPI, UploadFile, Form, Query, Response, Request
from starlette import status


from json_validata.__version__ import __version__
from json_validata.api.models import Metadata, ValidationResult, JobStatus
from json_validata.api.utils import _to_temporary_file
from json_validata.report.report import build_validation_result_from_report, \
    Report
from json_validata.task_queue import tasks
from json_validata.task_queue.server import app as celery_app


EXAMPLE_SCHEMA_URL = "https://schema.data.gouv.fr/schemas/etalab/schema-amenagements-cyclables/0.3.3/schema_amenagements_cyclables.json"  # nopep8
EXAMPLE_DATA_URL = "https://www.data.gouv.fr/fr/datasets/r/54f9ee93-76e4-4e02-afbf-7dd0c954355a"  # nopep8

api_description = (
     'The [Validata JSON](https://gitlab.com/validata-json/validata-json) API is a service for validating JSON files with respect to a given JSON Schema'
     )


app = FastAPI(
    title='Validata JSON API',
    description=api_description,
    version=__version__,
    servers=[{
        'url': 'https://json.validator.validata.fr/',
        'description': 'Hosts the JSON validation service.'
    },
        {
        'url': 'http://localhost:8000/',
        'description': 'Hosts the JSON validation service.'
    }],
    tags_metadata=[{"name": "validation"}, {"name": "status"}]
)


status_summary = 'Get the status of the API server'


@app.get(
    '/status',
    status_code=status.HTTP_200_OK,
    summary=status_summary,
    tags=['status'],
)
def status_endpoint() -> None:
    """Returns HTTP Status 200 if the server is up"""
    return


@app.get(
    '/',
    status_code=status.HTTP_303_SEE_OTHER,
    include_in_schema=False,
)
def status_redirect(response: Response) -> None:
    response.headers['location'] = '/status'


def _validation_job(
    data: Union[str, SpooledTemporaryFile],
    schema: str,
    data_is_remote: bool,
    metadata: Metadata,
    tags=['validation'],
) -> str:
    """ Queue a validation job with data and remote schema - Helper function

    * If `data_is_remote == True`, then `data` is expected to be an URL. Data
      is is stored as a `SpooledTemporaryFile` as a seekable file-type format
      is needed for reading it several times during json splitting.
    * If `data_is_remote == False`, then data is expected to be a
       `SpooledTemporaryFile`.

    Returns:
        str: job id
    """
    schema_file = request.urlopen(schema)
    data_path = _to_temporary_file(data, data_is_remote)
    return tasks.queue_job(schema_file, data_path, metadata)


url_validation_summary = (
    'Queue a validation job of remote GeoJSON data against '
    'a remote Jsonschema schema'
)


@ app.post(
    '/url_validation_job',
    response_model=str,
    status_code=status.HTTP_201_CREATED,
    summary=url_validation_summary,
    tags=['validation'],
)
async def url_validation_job(
    data: HttpUrl = Query(example=EXAMPLE_DATA_URL),
    schema: HttpUrl = Query(example=EXAMPLE_SCHEMA_URL),
    request: Request = None
) -> str:
    """
    Returns the validation job ID that can be used with:
    * `GET /job/{job_id}` endpoint to fetch the job status, or
    * `GET /job/{job_id}/output` endpoint to fetch the result once the job is
      finished

    See also `POST /file_validation_job` for uploading a local data file.
    """
    metadata = Metadata(request_url=str(request.url))
    return _validation_job(
        data,
        schema,
        data_is_remote=True,
        metadata=metadata,
    )

file_validation_summary = (
    'Queue a validation job of local GeoJSON data file against '
    'a remote Jsonschema schema'
)


@ app.post(
    '/file_validation_job',
    response_model=str,
    status_code=status.HTTP_201_CREATED,
    summary=file_validation_summary,
    tags=['validation'],
)
async def file_validation_job(
    data: UploadFile,
    schema: HttpUrl = Form(example=EXAMPLE_SCHEMA_URL),
    request: Request = None
) -> str:
    """
    Returns the validation job ID that can be used with:
    * `GET /job/{job_id}` endpoint to fetch the job status, or
    * `GET /job/{job_id}/output` endpoint to fetch the result once the job is
      finished

    See also `POST /url_validation_job` for validating a remote data file.
    """
    metadata = Metadata(request_url=str(request.url))
    return _validation_job(
        data.file,
        schema,
        data_is_remote=False,
        metadata=metadata
    )


get_job_id_summary = 'Fetch the status of a job'


@ app.get(
    '/job/{job_id}',
    summary=get_job_id_summary,
    tags=['validation'],
)
def get_job_id(job_id: str, response: Response) -> JobStatus:
    """
    Fetches the status of a job, with two fields:

    * "json_split_status" refers to the task of splitting the data into
      smaller chunks
    * "validation_status" refers to the task of validating the chunks and
      produce a validation report.

    When the validation has status 'SUCCESS', the response headers invite to a
    redirection towards  `GET /job/{job_id}/output` to fetch the full text
    report. Some clients may redirect automatically.

    The backend does not check if job is queued, so a wrong job id returns
    indefinitely:

    ```json
    {
      "job_status": {
        "json_split": "PENDING",
        "validation": "UNKNOWN"
      }
    }
    ```
    """
    job_status = JobStatus(
        **tasks.fetch_validation_job_status(job_id, celery_app)
    )
    if job_status.validation_status == 'SUCCESS':
        response.status_code = status.HTTP_303_SEE_OTHER
        response.headers['location'] = '/job/' + job_id + '/output'
    return job_status


fetch_validation_result_summary = \
    'Fetch the validation result for a given validation job'


@ app.get(
    '/job/{job_id}/output',
    summary=fetch_validation_result_summary,
    tags=['validation'],
)
def fetch_validation_result(
    job_id: str,
    text_or_json: Literal['text', 'json'] = Query(
        default='text',
        example='text',
        description='Whether the returned report should be full-text("text") or in jsonformat("json")'  # nopep8
    )
) -> ValidationResult:
    """
    Returns the validation report.
    If the job is not finished yet, then the endpoint will wait synchronously
    until the job is finished to return the report.

    The backend does not check if job is queued, so a wrong job id may
    indifenitely wait until the timeout(5 minutes without new data).
    """
    report_data = tasks.get_validation_report(job_id, celery_app)
    report = Report.from_json(report_data['report'])
    metadata = report_data['metadata']
    return build_validation_result_from_report(report, text_or_json, metadata)
