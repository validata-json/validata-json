import click
import json
import os
from urllib.request import urlopen

from json_validata.report.report import validate


@click.command()
@click.argument('data_file')
@click.option(
    '-s',
    '--schema',
    'schema_file',
    required=True,
    help='Path to the jsonschema to validate against'
)
def cli(data_file, schema_file):
    '''Validate data against a given jsonschema and print validation report'''
    schema = read_data(schema_file)
    data = read_data(data_file)
    click.echo(validate(data, schema))


def read_data(path):
    '''Read json data at "path", locally if file exists, remotely otherwise'''
    if os.path.exists(path):
        with open(path) as f:
            content = json.load(f)
    else:
        with urlopen(path) as f:
            content = json.load(f)
    return content


if __name__ == '__main__':
    cli()
