import heapq
from copy import deepcopy
from jinja2 import nativetypes
import json

from jsonschema._utils import find_additional_properties

from json_validata.report.path import Path
from json_validata.report.fr_locale import FrLocale, is_feminine
from json_validata.api import models

MAX_STORED_POSITIONS = 4


class ReportEntry(models.ReportEntry):
    """Class for keeping track of aggregated errors in a `Report`"""

    def __init__(
        self,
        positions,
        attribute_name,
        description,
        object_value,
        error_code,
    ):
        super().__init__(
            positions=positions,
            attribute_name=attribute_name,
            description=description,
            object_value=object_value,
            error_code=error_code,
            n_occurrences=1
        )

    def __str__(self):
        entry_str = (
            'Erreur survenue au niveau de l\'attribut '
            f'"{self.attribute_name}"\n'
            f'Position : {str(self.position)}\n'
            f'Description de l\'erreur : {self.description}\n'
            f'Valeur de l\'objet : {self.object_value}\n'
            f'Code erreur : {self.error_code}\n'
            f'{self.sprint_similar_entries()}'
        )
        return entry_str

    @property
    def position(self):
        if len(self.positions) == 0:
            raise IndexError
        return(self.positions[0])

    @property
    def other_positions(self):
        if len(self.positions) <= 1:
            return []
        else:
            return self.positions[1:]

    def should_be_merged_with(self, report_entry):
        return Path.differ_only_in_array_indexes(
            self.position,
            report_entry.position
        ) and self.error_code == report_entry.error_code

    def sprint_similar_entries(self):
        str_ = ''
        if self.n_occurrences == 1:
            return str_
        str_ += (
            f'Erreur similaire constatée à {self.n_occurrences - 1} '
            'autre(s) position(s)'
        )
        if self.n_occurrences <= MAX_STORED_POSITIONS:
            str_ += ' :\n'
            for pos in self.other_positions:
                str_ += f'  {pos}\n'
        else:
            str_ += '\n'
        return str_

    @classmethod
    def from_error(cls, error):
        return cls(
            positions=[Path(error.absolute_path)],
            attribute_name=Path.extract_attribute_name(error.path),
            description=get_description(error),
            object_value=json.dumps(error.instance),
            error_code=error.validator
        )

    @classmethod
    def from_dict(cls, entry):
        re = ReportEntry(
            [Path(pos) for pos in entry['positions']],
            entry['attribute_name'],
            entry['description'],
            entry['object_value'],
            entry['error_code']
        )
        re.n_occurrences = entry['n_occurrences']
        return re

    @classmethod
    def merge(cls, entry, other_entry):
        """Merges two similar entries

        Assumes similarity without checking.

        Returns:
          ReportEntry: merged entry
        """
        if entry.positions[0] < other_entry.positions[0]:
            ref_entry, additional_entry = (entry, other_entry)
        else:
            ref_entry, additional_entry = (other_entry, entry)
        new_entry = deepcopy(ref_entry)
        new_entry.n_occurrences += additional_entry.n_occurrences
        all_positions = list(
            # Using heapq as positions are ordered
            heapq.merge(new_entry.positions, additional_entry.positions)
        )
        new_entry.positions = all_positions[:MAX_STORED_POSITIONS]
        return new_entry


def get_description(error):
    """A human-readable, verbose, description of the error

    Returns:
      string: Error description, by default in French, if French description is
      missing then the original one
    """
    try:
        d = get_error_specific_data(error)
        env = nativetypes.NativeEnvironment()
        env.tests["feminine"] = is_feminine
        t = env.from_string(FrLocale.description(error.validator))
        return t.render(d)
    except ValueError:
        return error.message


def get_error_specific_data(error):
    """Get error specific data for validation error messages

    Returns:
      dict: validation error specific data.
      If error code is unknown, returns {}
    """
    code = error.validator
    if code == 'type':
        d = {
            'expected':
                FrLocale.jsonschema_type(error.validator_value),
            'got': FrLocale.type(error.instance)
        }
    elif code == 'additionalProperties':
        properties_list = list(find_additional_properties(
            error.instance,
            error.schema
        ))
        d = {
            'properties_str': json.dumps(properties_list).strip('[]'),
            'properties_list': properties_list
        }
    elif code == 'const':
        d = {'allowed': json.dumps(error.validator_value)}
    elif code == 'exclusiveMaximum':
        d = {'maximum': repr(error.validator_value)}
    elif code == 'exclusiveMinimum':
        d = {'minimum': repr(error.validator_value)}
    elif code == 'maximum':
        d = {'maximum': repr(error.validator_value)}
    elif code == 'minimum':
        d = {'minimum': repr(error.validator_value)}
    elif code == 'multipleOf':
        d = {'expected': repr(error.validator_value)}
    elif code == 'minItems':
        d = {
            'got': repr(len(error.instance)),
            'minimum': repr(error.validator_value)
        }
    elif code == 'maxItems':
        d = {
            'got': repr(len(error.instance)),
            'maximum': repr(error.validator_value)
        }
    elif code == 'uniqueItems':
        seen = set()
        dups = [x for x in error.instance
                if x in seen or
                seen.add(x)]
        d = {
            'duplicated': json.dumps(dups).strip('[]')
        }
    elif code == 'pattern':
        d = {'regex': json.dumps(error.validator_value)}
    elif code == 'minLength':
        d = {'minimum': error.validator_value}
    elif code == 'maxLength':
        d = {'maximum': error.validator_value}
    elif code == 'enum':
        d = {'allowed': json.dumps(error.validator_value).strip('[]')}
    elif code == 'required':
        d = {'key': json.dumps(error.validator_value).strip('[]')}
    elif code == 'minProperties':
        d = {'minimum': repr(error.validator_value)}
    elif code == 'maxProperties':
        d = {'maximum': repr(error.validator_value)}
    else:
        d = {}
    return d
