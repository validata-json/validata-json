# Needed for type hints inside class-methods
# See eg
# https://stackoverflow.com/questions/33533148/how-do-i-type-hint-a-method-with-the-type-of-the-enclosing-class
from __future__ import annotations

import json
from typing import List, Literal, Dict

import jsonschema

from json_validata.report.report_entry import ReportEntry
from json_validata.api import models


def validate(instance, schema, index_shift=0):
    """Validate a data instance against a given schema

    Instance and schema are expected to be provided as python objects.

    `index_shift` is used to shift all report entries.
    """
    validator = jsonschema.Draft7Validator(schema)
    errors = validator.iter_errors(instance)
    report = Report.from_errors(errors, index_shift)
    return report


def validate_to_json(instance, schema, index_shift=0):
    report = validate(instance, schema, index_shift)
    return report.to_json()


class Report(models.Report):
    """Class for aggregating the errors into a human readable report"""

    def __init__(self, auto_index_shift=0, array_to_shift=['features']):
        super().__init__(
            entries=[],
            auto_index_shift=auto_index_shift,
            array_to_shift=array_to_shift
        )

    def __str__(self):
        if not self.entries:
            return 'Les données sont valides par rapport au schéma JSON'
        report_str = ''
        for entry in self.entries[:-1]:
            report_str += str(entry) + '\n'
        report_str += str(self.entries[-1])
        return report_str

    def add_entry(self, new_entry):
        """Add an entry to the report

        Merge the entry to a similar existing entry if exists, otherwise adds
        a new entry to the report.

        Positions of merged entries should always be similar (ie only
        different by array indexes) and ordered.
        """
        new_entry.positions = [
            pos.shift(self.auto_index_shift, self.array_to_shift) for pos in
            new_entry.positions]
        for i, entry in enumerate(self.entries):
            if new_entry.should_be_merged_with(entry):
                self.entries[i] = ReportEntry.merge(entry, new_entry)
                return
        self.entries.append(new_entry)

    def to_json(self, **kwargs):
        print(f'{kwargs}')
        return super().json(**kwargs, include={'entries'})

    @classmethod
    def from_json(cls, json_report: str):
        report = cls()
        dict_report = json.loads(json_report)
        for entry in dict_report['entries']:
            report.add_entry(ReportEntry.from_dict(entry))
        return report

    @classmethod
    def from_errors(cls, errors, index_shift):
        report = cls(auto_index_shift=index_shift)
        for error in errors:
            new_candidate_entry = ReportEntry.from_error(error)
            report.add_entry(new_candidate_entry)
        return (report)

    @classmethod
    def merge(cls, reports: List[Report]) -> Report:
        """
        Merge reports into a consolidated one

        Return:
            Report: Consolidated Report

        Raises:
            ValueError: If empty list provided
        """
        if not reports:
            raise ValueError('merge_reports expects at least one report')

        merged_report = Report()
        for report in reports:
            for entry in report.entries:
                merged_report.add_entry(entry)

        return merged_report


def merge_reports(json_reports: List[str]) -> str:
    reports = [Report.from_json(r) for r in json_reports]
    return Report.merge(reports).to_json()


def build_validation_result_from_report(
    report: models.Report,
    text_or_json: Literal['text', 'json'],
    metadata: Dict
) -> models.ValidationResult:
    validation_report = str(report) if text_or_json == 'text' else report

    return models.ValidationResult(
        validation_report=validation_report,
        is_valid=(len(report.entries) == 0),
        metadata=models.Metadata(**metadata)
    )
