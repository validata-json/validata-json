
def _python_type_to_jsonschema_type(t):
    """Translate a python type to a jsonschema type

    Returns:
      string: jsonschema type
    Raises:
      TypeError: type does not match any jsonschema types
    """
    if t is str:
        return "string"
    elif t is float:
        return "number"
    elif t is int:
        return "integer"
    elif t is dict:
        return "object"
    elif t is list:
        return "array"
    elif t is bool:
        return "boolean"
    elif t is type(None):
        return "null"
    else:
        raise TypeError(
            "[validata-json] Le type python utilisé ne peut pas être "
            "converti en type jsonschema car il n'y a pas de correspondance "
            "connue"
        )
