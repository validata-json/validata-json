from .utils import _python_type_to_jsonschema_type


class FrLocale:

    UNNAMED_ATTRIBUTE = '(attribut sans nom)'
    ROOT = '(racine)'

    def jsonschema_type(t):
        """Translate a jsonschema type to french

        Returns:
            string.
        """
        translations = {
            "string": "chaîne de caractères",
            "number": "nombre décimal",
            "integer": "nombre entier",
            "object": "objet",
            "array": "tableau",
            "boolean": "booléen",
            "null": "objet `null`"
        }
        return translations[t]

    def type(o):
        """Translate jsonschema type in French of a python object

        Returns:
            string.
        """
        return FrLocale.jsonschema_type(
            _python_type_to_jsonschema_type(
                type(o)
            )
        )

    def description(error_code):
        """Get the template for the French description

        Returns:
          string.Template

        Raises:
          ValueError: unknown error code
        """
        error_templates = {
            'type': (
                'Type invalide. '
                'Un {%- if got is feminine -%} e {%- endif %} {{ got }} '
                'a été trouvé {%- if got is feminine -%} e {%- endif %}, '
                "alors qu'un {%- if expected is feminine -%} e {%- endif %} "
                '{{expected}} est attendu'
                '{%- if expected is feminine -%} e {%- endif %}.'
            ),
            'additionalProperties': (
                '{% set singular = (properties_list|length == 1) %}'
                "L'objet possède "
                '{% if singular -%}'
                '  un attribut'
                '{%- else -%}'
                '  {{ properties_list|length }} attributs'
                '{%- endif %} '
                '({{ properties_str }})'
                ' qui '
                '{% if singular -%}'
                "  n'est pas autorisé"
                '{%- else -%}'
                '  ne sont pas autorisés'
                '{%- endif %} '
                'par le schéma.'
            ),
            'const': 'Valeur attendue obligatoire: {{ allowed }}',
            'contains': (
                "Le tableau ne contient pas d'objet qui respecte "
                'le schéma attendu.'
            ),
            'exclusiveMaximum': (
                'La valeur doit être strictement '
                'inférieure à {{ maximum }}'
            ),
            'exclusiveMinimum': (
                'La valeur doit être strictement '
                'supérieure à {{ minimum }}'
            ),
            'maximum': (
                'La valeur doit être inférieure à '
                '{{ maximum }}'
            ),
            'minimum': (
                'La valeur doit être supérieure à {{minimum }}'
            ),
            'multipleOf': (
                'La valeur doit être un multiple de {{ expected }}'
            ),
            'minItems': (
                'Le tableau doit avoir une taille de plus de {{ minimum }} '
                '(taille observée : {{ got }})'
            ),
            'maxItems': (
                'Le tableau doit avoir une taille de moins de {{ maximum }} '
                '(taille observée : {{ got }})'
            ),
            'uniqueItems': (
                'Le tableau doit contenir des éléments uniques '
                '(élément(s) dupliqué(s) : {{ duplicated }})'
            ),
            'pattern': (
                "La chaîne de caractère doit respecter l'expression "
                'régulière {{ regex }}'
            ),
            'minLength': (
                'La chaîne de caractère doit être de longueur inférieure '
                'à {{ minimum }}'
            ),
            'maxLength': (
                'La chaîne de caractère doit être de longueur supérieure '
                'à {{ maximum }}'
            ),
            'enum': (
                'La valeur doit être parmi : '
                '{{ allowed }}'
            ),
            'required': (
                'L\'attribut {{ key }} est obligatoire et manquant'
            ),
            'minProperties': (
                "L'objet doit avoir au minimum {{ minimum }} attributs"
            ),
            'maxProperties': (
                "L'objet doit avoir au maximum {{ maximum }} attributs"
            ),
            'anyOf': (
                "Au moins l'un des schémas doit être validé "
                '(mot clé `anyOf`)'
            ),
            'oneOf': (
                "L'un des schémas, et seulement un, doit être validé "
                '(mot clé `oneOf`)'
            ),
            'not': (
                "Le schéma est validé alors qu'il ne devrait pas "
                '(mot clé `not`)'
            )
        }
        if error_code not in error_templates:
            raise ValueError
        return error_templates[error_code]


def is_feminine(jsonschema_type):
    """Return whether the French jsonschema_type is feminine

    Returns:
      bool.

    Raises:
      ValueError: unknown type.
    """
    is_feminine_dict = {
        "chaîne de caractères": True,
        "nombre décimal": False,
        "nombre entier": False,
        "objet": False,
        "tableau": False,
        "booléen": False,
        "objet `null`": False,
    }
    if jsonschema_type not in is_feminine_dict:
        raise ValueError

    return is_feminine_dict[jsonschema_type]
