from collections import deque

from json_validata.report.fr_locale import FrLocale


class Path(deque):
    """A path in a JSON object

    The path is printed according to the JSON path specification.
    It is stored as a `deque`.
    """

    def __str__(self):
        """Format the path to a position string

        Jsonpath like notation with custom ROOT string.

        Returns:
          string.
        """
        position = FrLocale.ROOT
        for elem in self:
            if isinstance(elem, int):
                position += "[" + str(elem) + "]"
            else:
                position += "." + elem
        return position

    def shift(self, index_shift, array_position):
        """Returns a copy of the path with specific array indexes shifted"""
        shifted_path = self.copy()

        if len(self) <= len(array_position):
            return shifted_path

        deviation = False
        for i, pos in enumerate(array_position):
            deviation = (self[i] != pos)
            if deviation:
                break

        array_index = len(array_position)
        if not deviation and isinstance(self[array_index], int):
            shifted_path[array_index] += index_shift
        return shifted_path

    def __strip_array_positions(self):
        """Creates a copy of the Position, where all array positions have been
        put to 0

        In pratice, array positions are all integer values

        Returns:
            Path.
        """

        return Path([0 if isinstance(s, int) else s for s in self])

    @classmethod
    def extract_attribute_name(cls, deque_path):
        """Extract the attribute name from a deque path

        It is define as the last element of the path that is not an integer, or
        the constant FrLocale.UNNAMED_ATTRIBUTE if none exists.
        """

        for elem in reversed(deque_path):
            if isinstance(elem, int):  # Array position
                continue
            else:
                return elem

        return FrLocale.UNNAMED_ATTRIBUTE

    @classmethod
    def differ_only_in_array_indexes(cls, path1, path2):
        return path1.__strip_array_positions() == \
            path2.__strip_array_positions()
