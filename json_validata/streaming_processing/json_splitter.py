from typing import BinaryIO, Optional, Iterator, \
    List, Dict, Any

from geojsplit import geojsplit
import geojson
import ijson
import time


def read_header(data_file: BinaryIO):
    """
    Extract all properties but 'features' from a geojson

    Return: dict
    """
    parser = ijson.parse(data_file)
    header = ijson.common.ObjectBuilder()

    for prefix, event, value in parser:
        if 'features' not in prefix:
            header.event(event, value)
    data_file.seek(0)
    return header.value


def split_json(data_file, batch_size=10):
    """
    Stream GeoJson file at `file_path` into batches.
    Every batch contains `batch_size` features
    and the headers properties.

    Return: dict
    """

    geojson = PureGeoJSONBatchStreamer(data_file)
    header = read_header(data_file)

    for data in geojson.stream(batch=batch_size):
        merged = {**header, **data}
        yield merged


class PureGeoJSONBatchStreamer(geojsplit.GeoJSONBatchStreamer):

    def __init__(self, geojson_file: BinaryIO) -> None:
        self.geojson_file = geojson_file

    def stream(
        self, batch: int, prefix: Optional[str] = None
    ) -> Iterator[geojson.feature.FeatureCollection]:
        """
        Generator method to yield batches of geojson Features in a Feature Collection.

        Args:
            batch (Optional[int], optional): The number of features in a single batch. Defaults to 100.
            prefix (Optional[str], optional): The prefix of the element of interest in the
                geojson document. Usually this should be `'features.item'`. Only change this if
                you now what you are doing. See https://github.com/ICRAR/ijson for more info.
                Defaults to `'features.item'`.

        Yields:
            (Iterator[geojson.feature.FeatureCollection]):
                The next batch of features wrapped in a new Feature Collection. This itself is
                just a subclass of a Dict instance, containing typical geojson attributes
                including a JSON array of Features. When `StopIteration` is raised, will yield
                whatever has been gathered so far in the `data` variable to ensure all
                features are collected.
        """
        if prefix is None:
            prefix = "features.item"

        features: Iterator[Dict[str, Any]] = ijson.items(
            self.geojson_file,
            prefix
        )
        try:
            while True:
                data: List[Dict[str, Any]] = []
                for _ in range(batch):
                    data.append(next(features))
                yield geojson.FeatureCollection(data)
        except StopIteration:
            if data:
                # yield remainder of data
                yield geojson.FeatureCollection(data)
            return


if __name__ == "__main__":
    batch_size = 10000

    start = time.time()
    with open('test/data/france-20220707.geojson', 'rb') as f:
        batch_iter = split_json(
            f,
            batch_size
        )
        for i in batch_iter:
            pass
    end = time.time()
    print(f'Time elapsed for batch size {batch_size}: {end - start}')
