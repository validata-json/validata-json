v0.1.1
======

* Dockerfile.api and Dockerfile.worker are merged into a single multistage
  Dockerfile, optimized for smaller image sizes.
* Update dependencies.


v0.1.0
======

* Initial release
