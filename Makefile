.PHONY: help clean clean-pyc clean-build list test test-all coverage docs release sdist gen-req

help:
	@echo "clean-build - remove build artifacts"
	@echo "clean-pyc - remove Python file artifacts"
	@echo "lint - check style with flake8"
	@echo "test - run tests quickly with the default Python"
	@echo "test-all - run tests on every Python version with tox"
	@echo "coverage - check code coverage quickly with the default Python"
	@echo "docs - generate Sphinx HTML documentation, including API docs"
	@echo "release - package and upload a release"
	@echo "sdist - package"
	@echo "gen-req - Generate requirements.txt file for users not using Poetry"

clean: clean-build clean-pyc

clean-build:
	rm -fr build/
	rm -fr dist/
	rm -fr *.egg-info

clean-pyc:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +

lint:
	flake8 json.validata test

test:
	poetry run pytest -vv

test-all:
	tox

coverage:
	coverage run --source json.validata setup.py test
	coverage report -m
	coverage html
	open htmlcov/index.html

docs:
	rm -f docs/json.validata.rst
	rm -f docs/modules.rst
	sphinx-apidoc -o docs/ json.validata
	$(MAKE) -C docs clean
	$(MAKE) -C docs html
	open docs/_build/html/index.html

release: clean
	python setup.py sdist upload
	python setup.py bdist_wheel upload

sdist: clean
	python setup.py sdist
	python setup.py bdist_wheel upload
	ls -l dist

serve-api:
	uvicorn --app-dir json_validata/api main:app --reload

run-celery:
	celery --workdir json_validata/task_queue/ -A server worker --loglevel=INFO

run-flower:
	celery --workdir json_validata/task_queue/ -A server flower

gen-req:
	poetry export -f requirements.txt --output requirements.txt --without-hashes
