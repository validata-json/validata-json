"""
Tests for `json.validata` module.
"""


from json_validata.streaming_processing import json_splitter


class TestJsonValidata(object):

    @classmethod
    def setup_class(cls):
        pass

    def test_read_header(self):
        with open('test/data/valid_test_file.json', 'rb') as data_file:
            header = json_splitter.read_header(data_file)
            assert header['crs']
            assert 'features' not in header

        with open('test/data/header_end_of_file.json', 'rb') as data_file:
            header = json_splitter.read_header(data_file)
            assert header['crs']

    def test_json_splitter(self):
        # File with 2 features
        file_path = 'test/data/valid_test_file.json'
        test_cases = [
            {
                'name': 'Check that the header is added',
                'file_path': file_path,
                'batch_size': 1,
                'expected_attribute': 'crs'
            },
            {
                'name': 'Several batches',
                'file_path': file_path,
                'batch_size': 1,
                'expected_features_length': 1,
            },
            {
                'name': 'Unique batch',
                'file_path': file_path,
                'batch_size': 2,
                'expected_features_length': 2,
            },
            {
                'name': 'Number of features exceeding batch size',
                'file_path': file_path,
                'batch_size': 3,
                'expected_features_length': 2,
            }
        ]

        for tc in test_cases:
            with open(tc['file_path'], 'rb') as file_:
                iterator = json_splitter.split_json(
                    data_file=file_,
                    batch_size=tc['batch_size']
                )
                data = next(iterator)
                if 'expected_attribute' in tc:
                    assert data[tc['expected_attribute']], tc['name']
                if 'expected_features_length' in tc:
                    assert len(data["features"]) == \
                        tc['expected_features_length'], \
                        tc['name']
