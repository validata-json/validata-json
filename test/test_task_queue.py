from io import StringIO
from unittest.mock import patch

from celery import shared_task
import pytest


from json_validata.task_queue import tasks


@shared_task(trail=True, max_retries=1)
def mock_validation_task(data, schema, index_shift=0):
    """Celery task for mocking purposes

    Returns a combination of the open street map ID of the first feature
    of the data and the index_shift.
    Use in conjunction with `mock_aggregation_task`
    """
    return {"features": data['features'], "index_shift": index_shift}


@shared_task(max_retries=1)
def mock_aggregation_task(mock_validations):
    """Celery task for mocking purposes

    Returns an aggregated string with the open street map ids returned by
    `mock_validation_task`s
    """
    return mock_validations


class TestMessaging(object):

    @pytest.mark.skip(reason="needs update")
    def test_queue_job(self, celery_session_app, celery_session_worker):
        # celery_session_app and celery_session_worker are pytest fixtures
        # defined by the "pytest-celery" package, are a local celery
        # application (running in-memory) and a celery worker started in a
        # separate thread. It is closed as soon as the test session ends.

        schema_file = StringIO('{}')
        data_file = StringIO('{"features": [0, 1, 2, 3]}')
        with \
                patch('json_validata.task_queue.tasks.validation_task',
                      new=mock_validation_task), \
                patch(
                    'json_validata.task_queue.tasks.aggregation_task',
                    new=mock_aggregation_task), \
                patch('builtins.open',
                      return_value=data_file):

            FAKE_PATH = ''

            g_id = tasks.queue_job(
                schema_file=schema_file,
                data_path=FAKE_PATH,
                batch_size=2
            )

        assert tasks.fetch_validation_job_status(g_id, celery_session_app) \
            == {'json_split': 'PENDING', 'validation': 'UNKNOWN'}

        mock_report = tasks.get_validation_report(g_id, celery_session_app)
        assert mock_report[0]['features'] == [0, 1]
        assert mock_report[0]['index_shift'] == 0
        assert mock_report[1]['features'] == [2, 3]
        assert mock_report[1]['index_shift'] == 2

        assert tasks.fetch_validation_job_status(g_id, celery_session_app) \
            == {'json_split': 'SUCCESS', 'validation': 'SUCCESS'}
