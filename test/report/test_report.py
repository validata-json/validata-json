"""
Tests for `json.validata` module.
"""

import json
from unittest.mock import patch

import pytest

from json_validata.api.models import ValidationResult, Metadata
from json_validata.report.path import Path
from json_validata.report.report import validate, Report, \
    build_validation_result_from_report
from json_validata.report.report_entry import ReportEntry


class HelperTestCase:
    """Helper class for defining a test case"""

    def __init__(self, subtest, schema, instance, expected,
                 with_index_shift=0):
        self.subtest = subtest
        self.schema = schema
        self.instance = instance
        self.expected = expected
        self.with_index_shift = with_index_shift

    def get_report(self):
        return validate(self.instance, self.schema, self.with_index_shift)

    def test_attribute(self, attribute):
        """Test a given attribute of the first entry of the report against the
        "expected" value
        """
        __tracebackhide__ = True
        report = self.get_report()
        assert len(
            report.entries) > 0, f'Expected at least 1 error(s). Got {len(report.entries)}.'
        assert getattr(report.entries[0],
                       attribute) == self.expected, f'Test attribute {attribute}: {self.subtest}'

    def test_fun(self, fun, test_name):
        """Test the first entry of the report, after applying `fun`, against
        the "expected" value
        """
        __tracebackhide__ = True
        report = self.get_report()
        assert len(
            report.entries) > 0, f'Expected at least 1 error(s). Got {len(report.entries)}.'
        assert fun(report.entries[
            0]) == self.expected, f'Test {test_name}: {self.subtest}'

    def test_report_str(self):
        """Test the printed report string"""
        __tracebackhide__ = True
        report = self.get_report()
        assert str(
            report) == self.expected, f'Test string report: {self.subtest}'


class TestReport(object):

    def test_valid_data(self):
        tc = HelperTestCase('Valid data message',
                            schema={'price': {'type': 'number'},
                                    'name': {'type': 'string'}, },
                            instance={'name': 'Eggs', 'price': 34.99},
                            expected=(
                                'Les données sont valides par rapport au '
                                'schéma JSON'))
        report = tc.get_report()
        assert not report.entries
        tc.test_report_str()

    def test_report_attribute_name(self):
        test_cases = [
            HelperTestCase('unnamed attribute', schema={'type': 'integer'},
                           instance=1.1, expected='(attribut sans nom)'),

            HelperTestCase('named attribute, depth 1',
                           schema={'type': 'object',
                                   'properties': {'foo': {'type': 'integer'}}},
                           instance={'foo': 1.1}, expected='foo'),
            HelperTestCase('named attribute, depth 2',
                           schema={'type': 'object', 'properties': {
                               'foo': {'type': 'object', 'properties': {
                                   'bar': {'type': 'integer'}}}}},
                           instance={'foo': {'bar': 1.1}}, expected='bar'),
            HelperTestCase('unnamed attribute, array', schema={'type': 'array',
                                                               'items': {
                                                                   'type': 'integer'}},
                           instance=[1, 1.1], expected='(attribut sans nom)')]

        for tc in test_cases:
            tc.test_attribute('attribute_name')

    def test_report_position(self):
        test_cases = [
            HelperTestCase('root position', schema={'type': 'integer'},
                           instance=1.1, expected='(racine)'),

            HelperTestCase('depth 1', schema={'type': 'object',
                                              'properties': {'foo': {'type': 'integer'}}},
                           instance={'foo': 1.1}, expected='(racine).foo'),

            HelperTestCase('depth 2', schema={'type': 'object', 'properties': {
                'foo': {'type': 'object',
                        'properties': {'bar': {'type': 'integer'}}}}},
                instance={'foo': {'bar': 1.1}}, expected='(racine).foo.bar'),

            HelperTestCase('array position',
                           schema={'type': 'array',
                                   'items': {'type': 'integer'}},
                           instance=[1, 1.1], expected='(racine)[1]'),

            HelperTestCase('index shift', schema={'properties': {
                'features': {'type': 'array', 'items': {'type': 'integer'}}}},
                instance={'features': [1, 1.1]},
                expected='(racine).features[2]', with_index_shift=1)]

        for tc in test_cases:
            tc.test_fun(lambda x: str(x.position), 'position string')

    def test_report_description_attribute(self):
        test_cases = [
            HelperTestCase('type 1', schema={'type': 'integer'}, instance=1.1,
                           expected=(
                               'Type invalide. Un nombre décimal a été trouvé, '
                               "alors qu'un nombre entier est attendu.")),
            HelperTestCase('type 2', schema={'type': 'string'}, instance=False,
                           expected=(
                               'Type invalide. Un booléen a été trouvé, '
                               "alors qu'une chaîne de caractères est attendue.")),
            HelperTestCase('additionalProperties 1', schema={'properties': {
                'foo': {'properties': {'bar': {}, 'fiz': {}},
                        'additionalProperties': False}}},
                instance={'foo': {'bar': 1, 'quux': 2}},
                expected=('L\'objet possède un attribut ("quux") '
                          "qui n'est pas autorisé par le schéma.")),
            HelperTestCase('additionalProperties 2', schema={'properties': {
                'foo': {'properties': {'bar': {}, 'fiz': {}},
                        'additionalProperties': False}}},
                instance={'foo': {'bar': 1, 'biz': 2}},
                expected=('L\'objet possède un attribut ("biz") '
                          "qui n'est pas autorisé par le schéma.")),
            HelperTestCase('additionalProperties 3', schema={'properties': {
                'foo': {'properties': {'bar': {}, 'fiz': {}},
                        'additionalProperties': False}}},
                instance={'foo': {'bar': 1, 'biz': 2, 'fix': 3}},
                expected=(
                'L\'objet possède 2 attributs ("biz", "fix") '
                "qui ne sont pas autorisés par le schéma.")),

            HelperTestCase('additionalProperties with patternProperties 1',
                           schema={'properties': {'foo': {
                               'patternProperties': {'^b': {}, '^f': {}},
                               'additionalProperties': False}}},
                           instance={'foo': {'bar': 1, 'quux': 2}},
                           expected=('L\'objet possède un attribut ("quux") '
                                     "qui n'est pas autorisé par le schéma.")),

            HelperTestCase('const 1',
                           schema={'properties': {'foo': {'const': 'bar'}}},
                           instance={'foo': 'quux'},
                           expected='Valeur attendue obligatoire: "bar"'),

            HelperTestCase('const 2', schema={
                'properties': {'foo': {'const': {'bar': 'quux'}}}},
                instance={'foo': 'quux'},
                expected='Valeur attendue obligatoire: {"bar": "quux"}'),

            HelperTestCase('contains 1', schema={'type': 'array', 'contains': {
                'type': 'number'}}, instance=['foo', 'bar'], expected=(
                "Le tableau ne contient pas d'objet qui respecte "
                'le schéma attendu.')),

            HelperTestCase('exclusiveMaximum 1',
                           schema={'type': 'number', 'exclusiveMaximum': 100},
                           instance=100, expected=(
                               'La valeur doit être strictement inférieure à 100')),
            HelperTestCase('exclusiveMaximum 2',
                           schema={'type': 'number', 'exclusiveMaximum': 100},
                           instance=101, expected=(
                               'La valeur doit être strictement inférieure à 100')),

            HelperTestCase('exclusiveMinimum 1',
                           schema={'type': 'number', 'exclusiveMinimum': 0},
                           instance=0, expected=(
                               'La valeur doit être strictement supérieure à 0')),
            HelperTestCase('exclusiveMinimum 2',
                           schema={'type': 'number', 'exclusiveMinimum': 0},
                           instance=-1, expected=(
                               'La valeur doit être strictement supérieure à 0')),

            HelperTestCase('maximum 2',
                           schema={'type': 'number', 'maximum': 100},
                           instance=101,
                           expected=('La valeur doit être inférieure à 100')),
            HelperTestCase('minimum', schema={'type': 'number', 'minimum': 0},
                           instance=-1,
                           expected=('La valeur doit être supérieure à 0')),

            HelperTestCase('multipleOf',
                           schema={'type': 'number', 'multipleOf': 3},
                           instance=4,
                           expected=('La valeur doit être un multiple de 3')),
            HelperTestCase('minItems', schema={'type': 'array', 'minItems': 2,
                                               'maxItems': 3}, instance=[1],
                           expected=(
                               'Le tableau doit avoir une taille de plus de 2 '
                               '(taille observée : 1)')),
            HelperTestCase('maxItems', schema={'type': 'array', 'minItems': 2,
                                               'maxItems': 3},
                           instance=[1, 2, 3, 4], expected=(
                'Le tableau doit avoir une taille de moins de 3 '
                '(taille observée : 4)')), HelperTestCase('uniqueItems 1',
                                                          schema={
                                                              'type': 'array',
                                                              'uniqueItems': True},
                                                          instance=[1, 2,
                                                                    4, 4],
                                                          expected=(
                                                              'Le tableau doit contenir des éléments uniques '
                                                              '(élément(s) dupliqué(s) : 4)')),
            HelperTestCase('uniqueItems 2',
                           schema={'type': 'array', 'uniqueItems': True},
                           instance=[1, 2, 4, 4, 1], expected=(
                               'Le tableau doit contenir des éléments uniques '
                               '(élément(s) dupliqué(s) : 4, 1)')),
            HelperTestCase('pattern', schema={'type': 'string',
                                              'pattern': '^(\\([0-9]{3}\\))?[0-9]{3}-[0-9]{4}$'},
                           instance='some text', expected=(
                "La chaîne de caractère doit respecter l'expression "
                r'régulière "^(\\([0-9]{3}\\))?[0-9]{3}-[0-9]{4}$"')),
            HelperTestCase('minLength',
                           schema={'type': 'string', 'minLength': 2,
                                   'maxLength': 3}, instance='s', expected=(
                               'La chaîne de caractère doit être de longueur inférieure '
                               'à 2')), HelperTestCase('maxLength',
                                                       schema={'type': 'string',
                                                               'minLength': 2,
                                                               'maxLength': 3},
                                                       instance='ssss', expected=(
                                                           'La chaîne de caractère doit être de longueur '
                                                           'supérieure à 3')), HelperTestCase('enum', schema={
                                                               'enum': ['red', 'amber', 'green', None, 42]}, instance=0,
                expected=(
                                                               'La valeur doit être parmi : '
                                                               '"red", "amber", "green", null, 42')),
            HelperTestCase('required', schema={'type': 'object',
                                               'properties': {'name': {
                                                   'type': 'string'}, },
                                               'required': ['name']},
                           instance={'email': 'a@b.c'}, expected=(
                'L\'attribut "name" est obligatoire et manquant')),
            HelperTestCase('minProperties',
                           schema={'type': 'object', 'minProperties': 2,
                                   'maxProperties': 3, },
                           instance={'email': 'a@b.c'}, expected=(
                               "L'objet doit avoir au minimum 2 attributs")),
            HelperTestCase('maxProperties',
                           schema={'type': 'object', 'minProperties': 2,
                                   'maxProperties': 3, },
                           instance={'email': 'a@b.c', 'nom': 'a',
                                     'prenom': '', 'surnom': ''}, expected=(
                               "L'objet doit avoir au maximum 3 attributs")),
            HelperTestCase('anyOf', schema={
                'anyOf': [{'type': 'string'}, {'type': 'number'}]},
                instance=True, expected=(
                "Au moins l'un des schémas doit être validé "
                    '(mot clé `anyOf`)')), HelperTestCase('oneOf', schema={
                        "oneOf": [{"type": "number", "multipleOf": 5},
                                  {"type": "number", "multipleOf": 3}]}, instance=15,
                expected=(
                        "L'un des schémas, et seulement un, doit être validé "
                        '(mot clé `oneOf`)')),
            HelperTestCase('not', schema={'not': {'type': 'string'}},
                           instance='some string', expected=(
                "Le schéma est validé alors qu'il ne devrait pas "
                '(mot clé `not`)'))]

        for tc in test_cases:
            tc.test_attribute('description')

    def test_object_value(self):
        test_cases = [
            HelperTestCase('1', schema={'type': 'integer'}, instance=1.1,
                           expected='1.1'

                           ), HelperTestCase('2', schema={'type': 'string'},
                                             instance={'key': 'value'},
                                             expected='{"key": "value"}')]
        for tc in test_cases:
            tc.test_attribute('object_value')

    def test_error_code(self):
        test_cases = [
            HelperTestCase('type', schema={'type': 'integer'}, instance=1.1,
                           expected='type'

                           ), HelperTestCase('required',
                                             schema={'type': 'object',
                                                     'properties': {'name': {
                                                         'type': 'string'}, },
                                                     'required': ['name']},
                                             instance={'email': 'a@b.c'},
                                             expected='required')]
        for tc in test_cases:
            tc.test_attribute('error_code')

    def test_validation_message(self):
        test_cases = [
            HelperTestCase('invalid type', schema={'type': 'integer'},
                           instance=1.1,
                           expected=("Erreur survenue au niveau de l'attribut "
                                     '"(attribut sans nom)"\n'
                                     'Position : (racine)\n'
                                     "Description de l'erreur : Type invalide. "
                                     'Un nombre décimal a été trouvé, '
                                     "alors qu'un nombre entier est attendu.\n"
                                     "Valeur de l'objet : 1.1\n"
                                     'Code erreur : type\n')),
            HelperTestCase('invalid additional properties', schema={
                "properties": {"foo": {"properties": {"bar": {}, "fiz": {}},
                                       "additionalProperties": False}}},
                           instance={"foo": {"bar": 1, "quux": 2}},
                           expected=("Erreur survenue au niveau de l'attribut "
                                     '"foo"\n'
                                     'Position : (racine).foo\n'
                                     "Description de l'erreur : L'objet possède un attribut "
                                     '("quux") qui n\'est pas autorisé par le schéma.\n'
                                     'Valeur de l\'objet : {"bar": 1, "quux": 2}\n'
                                     'Code erreur : additionalProperties\n')),
            HelperTestCase('allOf', schema={
                "allOf": [{"minLength": 2}, {"pattern": "^a"}]}, instance="b",
                expected=(
                'Erreur survenue au niveau de l\'attribut '
                '"(attribut sans nom)"\n'
                'Position : (racine)\n'
                'Description de l\'erreur : '
                'La chaîne de caractère doit être de longueur '
                'inférieure à 2\n'
                'Valeur de l\'objet : "b"\n'
                'Code erreur : minLength\n'
                '\n'
                'Erreur survenue au niveau de l\'attribut '
                '"(attribut sans nom)"\n'
                'Position : (racine)\n'
                'Description de l\'erreur : '
                'La chaîne de caractère doit respecter l\'expression '
                'régulière "^a"\n'
                'Valeur de l\'objet : "b"\n'
                'Code erreur : pattern\n')),

            HelperTestCase('Several occurrences of same entry',
                           schema={'type': 'array',
                                   'items': {'type': 'string'}},
                           instance=[1, 2, 'foo'], expected=(
                               'Erreur survenue au niveau de l\'attribut '
                               '"(attribut sans nom)"\n'
                               'Position : (racine)[0]\n'
                               'Description de l\'erreur : '
                               'Type invalide. Un nombre entier a été trouvé, alors '
                               'qu\'une chaîne de caractères est attendue.\n'
                               'Valeur de l\'objet : 1\n'
                               'Code erreur : type\n'
                               'Erreur similaire constatée à 1 autre(s) position(s) :\n'
                               '  (racine)[1]\n')),

            HelperTestCase('Several occurrences of same entry 2',
                           schema={'type': 'array',
                                   'items': {'type': 'string'}},
                           instance=[1, 2, 'three', 4, 'five', 6], expected=(
                               'Erreur survenue au niveau de l\'attribut '
                               '"(attribut sans nom)"\n'
                               'Position : (racine)[0]\n'
                               'Description de l\'erreur : '
                               'Type invalide. Un nombre entier a été trouvé, alors '
                               'qu\'une chaîne de caractères est attendue.\n'
                               'Valeur de l\'objet : 1\n'
                               'Code erreur : type\n'
                               'Erreur similaire constatée à 3 autre(s) position(s) :\n'
                               '  (racine)[1]\n'
                               '  (racine)[3]\n'
                               '  (racine)[5]\n')),

            HelperTestCase('Several occurrences of same entry 3',
                           schema={'type': 'array',
                                   'items': {'type': 'string'}},
                           instance=[1, 2, 'three', 4, 'five', 6, 7, 8, 9],
                           expected=('Erreur survenue au niveau de l\'attribut '
                                     '"(attribut sans nom)"\n'
                                     'Position : (racine)[0]\n'
                                     'Description de l\'erreur : '
                                     'Type invalide. Un nombre entier a été trouvé, alors '
                                     'qu\'une chaîne de caractères est attendue.\n'
                                     'Valeur de l\'objet : 1\n'
                                     'Code erreur : type\n'
                                     'Erreur similaire constatée à 6 autre(s) position(s)\n'))

        ]

        for tc in test_cases:
            tc.test_report_str()

    def test_to_json(self):
        re = ReportEntry([Path(["a", 2, "b"])], "attr_name", "desc", "obj_value",
                         "err_code")
        expected = '''\
{
  "entries": [
    {
      "attribute_name": "attr_name",
      "description": "desc",
      "error_code": "err_code",
      "n_occurrences": 1,
      "object_value": "obj_value",
      "positions": [
        [
          "a",
          2,
          "b"
        ]
      ]
    }
  ]
}'''
        report = Report()
        report.add_entry(re)

        assert report.to_json(indent=2, sort_keys=True) == expected

    def test_validate(self):
        data = json.load(open('test/data/valid_test_file.json'))
        schema = json.load(
            open('test/data/schema_amenagements_cyclables.json'))
        report = validate(data, schema)
        assert isinstance(report, Report)
        assert len(report.entries) == 0

        invalid_data = json.load(open('test/data/invalid_test_file.json'))

        report = validate(invalid_data, schema)
        assert isinstance(report, Report)
        assert len(report.entries) == 1

    def test_add_entry(self):
        ref_entry = ReportEntry([Path([])], '', '', '', '')
        entry_index_1 = ReportEntry([Path([1])], '', '', '', '')
        entry_index_2 = ReportEntry([Path([2])], '', '', '', '')

        test_cases = [{"subtest": "Add single entry", "entries": [ref_entry],
                       "expected_n_entries": 1, "position_first_entry": "(racine)", },
                      {"subtest": "Add two similar entries",
                       "entries": [entry_index_1, entry_index_2],
                       "expected_n_entries": 1,
                       "position_first_entry": "(racine)[1]", },
                      {"subtest": "Entry positions stay ordered",
                       "entries": [entry_index_2, entry_index_1],
                       "expected_n_entries": 1,
                       "position_first_entry": "(racine)[1]", }]

        for tc in test_cases:
            test_report = Report()
            for entry in tc["entries"]:
                test_report.add_entry(entry)
            assert len(test_report.entries) == tc["expected_n_entries"]
            assert str(test_report.entries[0].position) == tc[
                "position_first_entry"]


class TestReportAggregation(object):

    def test_report_shift(self):

        test_cases = [{'position_list': [], 'index_shift': 42, 'expected': [],
                       'array_to_shift': ['features']},
                      {'position_list': [0], 'index_shift': 42, 'expected': [0],
                       'array_to_shift': ['features']},
                      {'position_list': ['features', 0], 'index_shift': 42,
                       'expected': ['features', 42], 'array_to_shift': ['features']},
                      {'position_list': ['other_features', 0], 'index_shift': 42,
                       'expected': ['other_features', 42],
                       'array_to_shift': ['other_features']}, ]

        for tc in test_cases:
            r = Report(auto_index_shift=tc['index_shift'],
                       array_to_shift=tc['array_to_shift'])

            re = ReportEntry([Path(tc['position_list'])], '', '', '', '')
            r.add_entry(re)
            assert r.entries[0].positions[0] == Path(tc['expected'])

    def test_merge_reports(self):
        with pytest.raises(ValueError):
            assert Report.merge(
                []), 'At least one Report is expected to operate a merge'

        def _report(report_entries_position):
            report = Report()
            report.entries = [ReportEntry([pos], '', '', '', '') for pos in
                              report_entries_position]
            return report

        reference_report = _report([Path([0])])
        report_with_entry_not_to_merge = _report([Path(['features', 1])])
        report_two_entries = _report([Path([1]), Path(['a'])])
        other_report_two_entries = _report([Path(['b']), Path(['c'])])

        test_cases = [{'test_name': 'Merge empty reports',
                       'reports': [Report(), Report()], 'expected_n_entries': 0},
                      {'test_name': 'Merge single report', 'reports': [reference_report],
                       'expected_n_entries': 1},
                      {'test_name': 'Merge filled report with empty one',
                       'reports': [reference_report, Report()],
                       'expected_n_entries': 1},
                      {'test_name': 'Merge entries which should be merged 1',
                       'reports': [reference_report, reference_report],
                       'expected_n_entries': 1},
                      {'test_name': 'Merge entries which should be merged 2',
                       'reports': [reference_report, report_two_entries],
                       'expected_n_entries': 2}, {'test_name': 'No merge 1',
                                                  'reports': [reference_report, report_with_entry_not_to_merge],
                                                  'expected_n_entries': 2}, {'test_name': 'No merge 2',
                                                                             'reports': [report_two_entries, other_report_two_entries],
                                                                             'expected_n_entries': 4}]

        for tc in test_cases:
            merged_reports = Report.merge(tc['reports'])
            assert len(merged_reports.entries) == tc['expected_n_entries'], tc[
                'test_name']

    def test_from_json(self):

        #  Optional : it is possible to test a specific attribute of the first
        #  entry with 'test_attribute' and 'extract_attribute'
        test_cases = [
            {
                'name': 'Empty report',
                'json_report': '{"entries": []}',
                'expected_n_entries': 0,
            },
            {
                'name': 'Report with one dummy entry',
                'json_report': (
                    '{"entries": [{"positions": [[]], "attribute_name": "",'
                    '"description": "", "object_value": "", "error_code": "", '
                    '"n_occurrences": 1}]}'
                ),
                'expected_n_entries': 1,
            },
            {
                'name': 'Report with a different entry',
                'json_report': (
                    '{"entries": [{"positions": [[]], "attribute_name": "",'
                    '"description": "entry description", "object_value": "",'
                    '"error_code": "", "n_occurrences": 1}]}'
                ),
                'expected_n_entries': 1,
                'test_attribute': 'description',
                'expected_attribute': 'entry description'
            },
            {
                'name': 'Report with a different position',
                'json_report': (
                    '{"entries": [{"positions": [["a", 2]], '
                    '"attribute_name": "",'
                    '"description": "entry description", "object_value": "",'
                    '"error_code": "", "n_occurrences": 1}]}'
                ),
                'expected_n_entries': 1,
                'test_attribute': 'positions',
                'expected_attribute': [Path(["a", 2])]
            },
            {
                'name': 'Report with several occurrences',
                'json_report': (
                    '{"entries": [{"positions": [["a", 2]], '
                    '"attribute_name": "",'
                    '"description": "entry description", "object_value": "",'
                    '"error_code": "", "n_occurrences": 6}]}'
                ),
                'expected_n_entries': 1,
                'test_attribute': 'n_occurrences',
                'expected_attribute': 6
            }
        ]

        for tc in test_cases:
            report = Report.from_json(tc['json_report'])
            assert isinstance(report, Report), tc['name']
            assert len(report.entries) == tc['expected_n_entries'], tc['name']
            if 'test_attribute' in tc:
                assert getattr(report.entries[0], tc['test_attribute']) == \
                    tc['expected_attribute'], tc['name']


def test_build_validation_result():

    with patch('json_validata.report.report_entry.ReportEntry') as mock_entry:
        report_with_entry = Report()
        report_with_entry.add_entry(mock_entry)

    test_cases = [Report(), report_with_entry]
    m = Metadata(request_url='http://request').dict()
    for r in test_cases:
        vr = build_validation_result_from_report(r, 'json', m)
        assert isinstance(vr, ValidationResult)
        assert vr.validation_report == r

        assert vr.is_valid == (len(r.entries) == 0)

        assert hasattr(vr, 'metadata')

        assert hasattr(vr.metadata, 'json_validata_version') and \
            vr.metadata.json_validata_version

        assert hasattr(vr.metadata, 'timestamp') and \
            vr.metadata.timestamp

        assert hasattr(vr.metadata, 'jsonschema_version') and \
            vr.metadata.jsonschema_version

    for r in test_cases:
        vr = build_validation_result_from_report(r, 'text', m)
        assert isinstance(vr, ValidationResult)
        assert vr.validation_report == str(r)
