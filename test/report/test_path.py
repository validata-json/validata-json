from json_validata.report.path import Path


class TestPath(object):

    def test_differ_only_in_array_indexes(self):
        test_cases = [
            {'p1': [], 'p2': [], 'expected': True},
            {'p1': [], 'p2': ['a'], 'expected': False},
            {'p1': [], 'p2': [42], 'expected': False},
            {'p1': [0], 'p2': [42], 'expected': True},
            {'p1': ['a', 0], 'p2': ['a', 42], 'expected': True},
            {'p1': ['a', 0, 3], 'p2': ['a', 42, 12], 'expected': True},
            {'p1': ['a', 3, 'b'], 'p2': ['a', 12, 'c'], 'expected': False},
            # 'a[3]' is a valid JSON key
            {'p1': ['a[3]'], 'p2': ['a', 12], 'expected': False},
            # '3' is a valid JSON key
            {'p1': ['a', '3'], 'p2': ['a', 12], 'expected': False},
        ]

        for tc in test_cases:
            assert Path.differ_only_in_array_indexes(Path(tc['p1']),
                                                     Path(tc['p2'])) == \
                tc['expected']

    def test_shift(self):
        p = Path([1])
        p.shift(3, [])
        assert p == Path([1]), 'shift does not mutate its argument'

        test_cases = [
            {
                'p': [],
                'index_shift': 3,
                'array_position': [],
                'expected': []
            },
            {
                'p': [1],
                'index_shift': 3,
                'array_position': [],
                'expected': [4]
            },
            {
                'p': [1],
                'index_shift': 3,
                'array_position': ['a'],
                'expected': [1]
            },
            {
                'p': ['a', 1],
                'index_shift': 3,
                'array_position': ['a'],
                'expected': ['a', 4]
            },
            {
                'p': ['a', 1, 2],
                'index_shift': 3,
                'array_position': ['a'],
                'expected': ['a', 4, 2]
            },
            {
                'p': ['a', 1, 'b', 2],
                'index_shift': 3,
                'array_position': ['a'],
                'expected': ['a', 4, 'b', 2]
            },
            {
                'p': ['a', 'b', 'c', 1, 2],
                'index_shift': 3,
                'array_position': ['a', 'b', 'c'],
                'expected': ['a', 'b', 'c',  4, 2]
            },
            {
                'p': ['a', 'b', 'c', 1, 2],
                'index_shift': 3,
                'array_position': ['a', 'b'],
                'expected': ['a', 'b', 'c', 1, 2]
            },
            {
                'p': [],
                'index_shift': 3,
                'array_position': ['a', 'b'],
                'expected': []
            },

        ]

        for tc in test_cases:
            assert Path(tc['p']).shift(
                tc['index_shift'],
                tc['array_position']
            ) == Path(tc['expected'])
