
from mock import patch

from json_validata.report.path import Path
from json_validata.report.report_entry import ReportEntry


class TestReportEntry(object):

    def test_should_be_merged(self):

        def _test_entry(position_list, error_code):
            return ReportEntry(
                positions=[Path(position_list)],
                attribute_name='', description='', object_value='',
                error_code=error_code)

        test_cases = [
            {
                'entry1': _test_entry(['foo', 2], 'type'),
                'entry2': _test_entry(['foo', 5], 'type'),
                'expected': True
            },
            {
                'entry1': _test_entry(['foo', 2], 'type'),
                'entry2': _test_entry(['other_path'], 'type'),
                'expected': False
            },
            {
                'entry1': _test_entry(['foo', 2], 'type'),
                'entry2': _test_entry(['foo', 5], 'other_type'),
                'expected': False
            }
        ]

        for tc in test_cases:
            assert tc['entry2'].should_be_merged_with(tc['entry1']) == \
                tc['expected']

    def test_merge(self):

        def _test_entry_several_positions(array_indexes):
            re = ReportEntry([Path([])], '', '', '', '')
            re.positions = [Path([i]) for i in array_indexes]
            re.n_occurrences = len(array_indexes)
            return re

        entry1 = _test_entry_several_positions([1])
        entry2 = _test_entry_several_positions([2])
        merged_entry = ReportEntry.merge(entry1, entry2)
        assert entry1.positions == [Path([1])] and \
            entry2.positions == [Path([2])], \
            'merge should not mutate arguments'

        test_cases = [{
            'name': 'Simple case',
            'array_indexes1': [1],
            'array_indexes2': [2],
            'expected_pos_indexes': [1, 2],
            'expected_n_occurrences':2
        }, {
            'name': 'merge should keep previous positions (left)',
            'array_indexes1': [1, 2],
            'array_indexes2': [3],
            'expected_pos_indexes': [1, 2, 3],
            'expected_n_occurrences': 3
        }, {
            'name': 'merge should keep previous positions (right)',
            'array_indexes1': [1],
            'array_indexes2': [2, 3],
            'expected_pos_indexes': [1, 2, 3],
            'expected_n_occurrences': 3
        }, {
            'name': 'merge should keep positions ordered 1',
            'array_indexes1': [2],
            'array_indexes2': [1],
            'expected_pos_indexes': [1, 2],
            'expected_n_occurrences': 2
        }, {
            'name': 'merge should keep positions ordered 2',
            'array_indexes1': [1, 3],
            'array_indexes2': [2],
            'expected_pos_indexes': [1, 2, 3],
            'expected_n_occurrences': 3
        }, {
            'name': 'merge should keep positions ordered 3',
            'array_indexes1': [2, 3],
            'array_indexes2': [1, 4],
            'expected_pos_indexes': [1, 2, 3, 4],
            'expected_n_occurrences': 4
        }, {
            'name': 'merge should not keep more than MAX_STORED_POSITION 1',
            'array_indexes1': [2, 3],
            'array_indexes2': [1, 4],
            'test_max_stored_positions': 3,
            'expected_pos_indexes': [1, 2, 3],
            'expected_n_occurrences': 4,
        }, {
            'name': 'merge should not keep more than MAX_STORED_POSITION 2',
            'array_indexes1': [2, 3],
            'array_indexes2': [1, 4],
            'test_max_stored_positions': 2,
            'expected_pos_indexes': [1, 2],
            'expected_n_occurrences': 4,
        }
        ]

        for tc in test_cases:
            if 'test_max_stored_positions' in tc:
                test_max_stored_positions = tc['test_max_stored_positions']
            else:
                test_max_stored_positions = 100

            with patch(
                'json_validata.report.report_entry.MAX_STORED_POSITIONS',
                test_max_stored_positions
            ):

                expected_positions = [Path([i])
                                      for i in tc['expected_pos_indexes']]
                merged_entry = ReportEntry.merge(
                    _test_entry_several_positions(tc['array_indexes1']),
                    _test_entry_several_positions(tc['array_indexes2']),
                )
                assert merged_entry.positions == expected_positions, tc['name']
                assert merged_entry.n_occurrences == \
                    tc['expected_n_occurrences'], tc['name']

    def test_merge_should_have_correct_object_value(self):
        re1 = ReportEntry([Path(['a', 1])], '', '', 1, '')
        re2 = ReportEntry([Path(['a', 2])], '', '', 2, '')
        re_merged = ReportEntry.merge(re1, re2)

        assert re_merged.object_value == '1'

        re_merged_2 = ReportEntry.merge(re2, re1)
        assert re_merged_2.object_value == '1'
