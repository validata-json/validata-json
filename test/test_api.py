from dataclasses import dataclass
import io
import os
from tempfile import SpooledTemporaryFile
from typing import Union, Dict, Optional
from unittest.mock import patch
from urllib.parse import urlencode

from fastapi.testclient import TestClient
from json_validata.api import main, utils

client = TestClient(main.app)


FAKE_JOB_ID = 'abc'
FAKE_URL = 'http://url.com'


def _patch_validation_job():
    return patch(
        'json_validata.api.main._validation_job',
        side_effect=FAKE_JOB_ID
    )


def test_url_validation_job():
    test_cases = [
        {
            'name': 'valid input',
            'data': 'http://example.com/data',
            'schema': 'http://examle.com/schema',
            'raw': False,
            'expected_status_code': 201,
        },
        {
            'name': 'invalid url for data and schema',
            'data': 'data',
            'schema': 'schema',
            'raw': False,
            'expected_status_code': 422,
        },
        {
            'name': 'missing optional raw parameter',
            'data': 'http://example.com/data',
            'schema': 'http://examle.com/schema',
            'expected_status_code': 201,
        },
        {
            'name': 'missing schema parameter',
            'data': 'http://example.com/data',
            'expected_status_code': 422,
        },
        {
            'name': 'missing data parameter',
            'schema': 'http://example.com/schema',
            'expected_status_code': 422,
        }
    ]

    def build_path(parameters):
        path = '/url_validation_job'
        if parameters:
            path += '?' + urlencode(parameters)
        return path

    with _patch_validation_job():
        for tc in test_cases:
            path = build_path(
                {
                    k: v for (k, v) in tc.items()
                    if k in ['data', 'schema', 'raw']
                }
            )
            response = client.post(path)
            assert response.status_code == tc['expected_status_code'], \
                tc['name']


def test_file_validation_job():
    FAKE_FILE_ARG = {"data": ("filename.csv", '{}')}

    @dataclass
    class TestCase():
        name: str
        schema_url: str
        file_: Dict
        expected_status_code: Optional[int] = None
        check_validation_args: bool = False

        def api_call(self):
            payload = {'schema': self.schema_url}
            with _patch_validation_job() as mock_validation:
                response = client.post(
                    '/file_validation_job',
                    data=payload,
                    files=self.file_
                )
            return response, mock_validation

        def assert_status_code(self):
            if self.expected_status_code:
                response = self.api_call()[0]
                assert response.status_code == self.expected_status_code, \
                    self.name

        def assert_validation_call_args(self):
            if self.check_validation_args:
                mock_validation = self.api_call()[1]
                args = mock_validation.call_args.args
                data_arg = args[0]
                assert isinstance(
                    data_arg,
                    (SpooledTemporaryFile, io.BufferedIOBase)
                ), self.name

    test_cases = [
        TestCase(
            name='Standard API call',
            schema_url=FAKE_URL,
            file_=FAKE_FILE_ARG,
            expected_status_code=201,
        ),
        TestCase(
            name='Missing file in form',
            schema_url=FAKE_URL,
            file_={},
            expected_status_code=422
        ),
        TestCase(
            name='API calls validation with right argument types',
            schema_url=FAKE_URL,
            file_=FAKE_FILE_ARG,
            check_validation_args=True
        )
    ]
    for tc in test_cases:
        tc.assert_status_code()
        tc.assert_validation_call_args()


def test_to_temporary_file():
    FAKE_BYTES = b'{"stuff": 42}'
    FAKE_BYTES_STREAM = io.BytesIO(FAKE_BYTES)

    @dataclass
    class TestCase():
        name: str
        data: Union[str, io.BufferedIOBase, SpooledTemporaryFile]
        data_is_remote: bool
        _returned_value: io.IOBase = None

        @property
        def returned_value(self):
            if not self._returned_value:
                self._written_bytes = io.BytesIO()
                with \
                        patch('urllib.request.urlopen',
                              return_value=FAKE_BYTES_STREAM):

                    self._returned_value = utils._to_temporary_file(
                        self.data,
                        self.data_is_remote
                    )
            return self._returned_value

        def assert_file_exists(self):
            assert os.path.exists(self.returned_value)

        def assert_accessible_file_content(self):
            with open(self.returned_value, 'rb') as f:
                written_bytes = f.read()
            assert written_bytes == FAKE_BYTES

        def rm_file(self):
            os.remove(self.returned_value)

    test_cases = [
        # TestCase(
        #     name='Local starlette.datastructures.UploadFile data',
        #     data=SpooledTemporaryFile(),
        #     data_is_remote=False
        # ),
        TestCase(
            name='Url for remote data',
            data=FAKE_URL,
            data_is_remote=True
        ),
    ]

    for tc in test_cases:
        try:
            tc.assert_file_exists()
            tc.assert_accessible_file_content()
        finally:
            tc.rm_file()
