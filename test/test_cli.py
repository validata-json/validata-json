from io import StringIO
import json
from pydantic import BaseModel, Field
import pytest
from typing import Optional, Any
from unittest.mock import patch

from click.testing import CliRunner

from json_validata.cli.main import cli, read_data

FAKE_SCHEMA_PATH = './schema'
FAKE_DATA_PATH = './data'
FAKE_SCHEMA_CONTENT = {'schema': ''}
FAKE_DATA_CONTENT = {'data': ''}
FAKE_DATA_JSON = json.dumps(FAKE_DATA_CONTENT)


@pytest.fixture(scope="module")
def runner():
    return CliRunner()


class TestCLI(object):

    def test_cli_arguments(self, runner):
        test_cases = [
            {
                'name': 'Cli runs with data option and file argument',
                'arguments': ['--schema=' + FAKE_SCHEMA_PATH, FAKE_DATA_PATH],
                'expected_exit_code': 0
            },
            {
                'name': 'Cli supports short schema option -s',
                'arguments': ['-s=' + FAKE_SCHEMA_PATH, FAKE_DATA_PATH],
                'expected_exit_code': 0
            },
            {
                'name': 'Cli fails with missing schema',
                'arguments': [FAKE_DATA_PATH],
                'expected_exit_code': 2
            },
            {
                'name': 'Cli fails with missing data',
                'arguments': ['--schema=' + FAKE_SCHEMA_PATH],
                'expected_exit_code': 2
            }
        ]

        with \
                patch('json_validata.cli.main.read_data'), \
                patch('json_validata.cli.main.validate'):

            for tc in test_cases:
                result = runner.invoke(cli, tc['arguments'])
                assert result.exit_code == tc['expected_exit_code'], \
                    tc['name']

    def test_cli_behavior(self, runner):

        def fake_content(path):
            if path == FAKE_DATA_PATH:
                return FAKE_DATA_CONTENT
            elif path == FAKE_SCHEMA_PATH:
                return FAKE_SCHEMA_CONTENT

        with \
                patch('json_validata.cli.main.read_data', wraps=fake_content) as mock_read_data, \
                patch('json_validata.cli.main.validate') as mock_validate:

            runner.invoke(cli, ['--schema='+FAKE_SCHEMA_PATH, FAKE_DATA_PATH])

            mock_read_data.assert_any_call(FAKE_SCHEMA_PATH)
            mock_read_data.assert_any_call(FAKE_DATA_PATH)
            mock_validate.assert_called_once_with(
                FAKE_DATA_CONTENT,
                FAKE_SCHEMA_CONTENT
            )

        with \
                patch('json_validata.cli.main.read_data') as mock_read_data, \
                patch('json_validata.cli.main.validate', return_value='abc'):
            result = runner.invoke(
                cli, ['--schema='+FAKE_SCHEMA_PATH, FAKE_DATA_PATH])

            assert result.output == 'abc\n'

    def test_read_data(self):

        class TC(BaseModel):
            '''Helper to setup a test case for `read_data` function.'''
            is_local: bool = Field(
                description='Whether a path to an existing local file should be mocked'
            )
            local_file_opened: Optional[bool] = Field(
                description='Check if one attempt to open a local file'
            )
            remote_file_opened: Optional[bool] = Field(
                description='Check if one attempt to open a remote file'
            )
            file_content: str = Field(
                '{}',
                description='Mocked file content'
            )
            expected_data: Optional[Any] = Field(
                description='Expected return data'
            )

            @classmethod
            def assert_calls(cls, mock, should_be_called):
                if should_be_called is not None:
                    if should_be_called:
                        mock.assert_called_once_with(FAKE_DATA_PATH)
                    else:
                        mock.assert_not_called()

        test_cases = [
            TC(
                is_local=True,
                local_file_opened=True,
                remote_file_opened=False
            ),
            TC(
                is_local=False,
                local_file_opened=False,
                remote_file_opened=True
            ),
            TC(
                is_local=False,
                file_content=FAKE_DATA_JSON,
                expected_data=FAKE_DATA_CONTENT
            ),
            TC(
                is_local=True,
                file_content=FAKE_DATA_JSON,
                expected_data=FAKE_DATA_CONTENT
            ),
        ]

        for tc in test_cases:
            with \
                    patch('os.path.exists', return_value=tc.is_local), \
                    patch(
                        'json_validata.cli.main.open',
                        return_value=StringIO(tc.file_content)
                    ) as local, \
                    patch(
                        'json_validata.cli.main.urlopen',
                        return_value=StringIO(tc.file_content)
                    ) as remote:

                data = read_data(FAKE_DATA_PATH)

                TC.assert_calls(local, tc.local_file_opened)
                TC.assert_calls(remote, tc.remote_file_opened)
                if tc.expected_data is not None:
                    assert data == tc.expected_data
